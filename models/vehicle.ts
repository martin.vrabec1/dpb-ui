export type Vehicle = {
    azimuth: number | null;
    agencyId: string | null;
    number: number | null;
    routeShortName: string | null;
    routeLongName: string | null;
    latitude: string | null;
    longitude: string | null;
    bearing: number | null;
    type: string | null;
    lastUpdate: number | null;
    line: string | null;
    transportType: string | null;
  };