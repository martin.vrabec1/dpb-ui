import type { Vehicle } from "~/models/vehicle";

export const useFilterState = defineStore('filterStore', {
    state: () => ({
      filter: {
        transportType: ["bus", "tram", "trolley"],
        transportLines: [],
        transportTypeStates: {
            bus: true,
            tram: true,
            trolley: true
        }
      }
    }),
    actions: {
        getBooleanState(type: string, key:string) {   
            const filter = this.filter[type];
            const in_array = filter.includes(key);
            return in_array;
        },
        getState(type: string, key:string) {   
            const state = this.getBooleanState(type, key);
            return state === true ? "on" : "off";
        },
        toggleFromFilter(type: string, key: string){
            const state = this.getBooleanState(type, key);
            if (state){
                this.filter[type] = this.filter[type].filter(function(i){
                    return i !== key;
                })
            } else {
                this.filter[type].push(key);
            }
        },
        toggleTransportTypeState(type){
            this.filter.transportTypeStates[type] = !this.filter.transportTypeStates[type];
            return this.filter.transportTypeStates[type];
        },
        getTransportTypeState(type){
            return this.filter.transportTypeStates[type];
        },
        setState(type: string, key:string, value: any) {
            if (value === "on"){
                this.filter[type].push(key);
            } else {
                this.filter[type] = this.filter[type].filter(function(i){
                    return i !== key;
                })
            }
        },
        validate(vehicle: Vehicle){
            //transport type
            if (vehicle?.transportType && !this.filter.transportType?.includes(vehicle?.transportType)){
                return false;
            }
            if (vehicle?.line && !this.filter.transportLines?.includes(vehicle?.line)){
                return false;
            }
            return true;
        }
    }
}) 