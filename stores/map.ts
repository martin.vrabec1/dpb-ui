import config from "~/config"
import type { Vehicle } from "~/models/vehicle";

export const useMapStore = defineStore('mapStore', {
    state: () => ({
      vehicles: [] as Vehicle[]
    }),
    actions: {
      fetchVehicles(callback?) {
        const url = config.api.host + "/vehicle";
        fetch(url).then((response) => {
            this.vehicles = [];
            response.json().then((vehicles)=>{
              this.vehicles = vehicles
              if (callback){
                callback()
              }
            });
        }) 
      }
    }
  })