export default
{
    api: {
        host: "http://localhost:8080/api"
    },
    map: {
        center: {
            lat: 48.1549,
            lng: 17.129043,
        },
        zoom: 14,
    },
    transportTypeFilter: 
        {
            tram: {on: "icons/tram.svg", off: "icons/tram-off.svg"},
            trolley: {on: "icons/trolley.svg", off: "icons/trolley-off.svg"},
            bus: {on: "icons/bus.svg", off: "icons/bus-off.svg"}
        }
    
}